/*** REGION 1 */
"use strict";

    const gREQUEST_STATUS_OK = 200; // GET & PUT success
    const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
    const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
    const gREQUEST_CREATE_SUCCESS = 201;
    // bạn có thê dùng để lưu trữ combo được chọn, mỗi khi khách chọn bạn lại đổi giá trị properties của nó
    var gSelectedMenuStructure = {
      menuName: "",    // S, M, L
      duongKinhCM: 0,
      suongNuong: 0,
      saladGr: 0,
      drink: 0,
      priceVND: 0
    }
    // bạn có thể dùng để lưu loại pizza đươc chọn, mỗi khi khách chọn, bạn lại đổi giá trị cho nó
    var gSelectedPizzaType = ""
    var gOder = {
    menuDuocChon: "",
    loaiPizza: "",
    hoVaTen: "",
    email: "",
    dienThoai: "",
    diaChi: "",
    loiNhan: "",
    voucherId: "",
    priceAnnualVND(){
      return gSelectedMenuStructure.priceVND * ( 1 - gDiscount *  0.01 );  
    }
  }
  var gDiscountVouchers = {};

  var gDiscount = 0;

  //hàm cho task 27.80
  var gObjectRequest = {
    kichCo : "",
    duongKinh : "",
    suon : "",
    salad : "",
    loaiPizza : "",
    idVourcher : "",
    idLoaiNuocUong : "",
    soLuongNuoc : "",
    hoTen : "",
    thanhTien : "",
    email : "",
    soDienThoai : "",
    diaChi : "",
    loiNhan : "", 
  }
  $(document).ready(function(){
        onPageLoading();
        $("#btn-small").on("click", function() {
            onBtnSmallClick();
        });
        $("#btn-medium").on("click", function() {
            onBtnMediumClick();
        });
        $("#btn-large").on("click", function() {
            onBtnLargeClick();
        });
        $("#btn-ocean").on("click", function() {
            onBtnOceanClick();
        });
        $("#btn-hawai").on("click", function() {
            onBtnHawaiClick();
        });
        $("#btn-bacon").on("click", function() {
            onBtnBaconClick();
        });
        $("#btn-gui").on("click", function() {
            onBtnGuiClick();
        });
        $("#btn-tao-don").on("click", function() {
            onBtnTaoDonClick();
        });

    });
/*** REGION 2 */
function onPageLoading(){
    "use strict"
    //Gọi hàm load loại đồ uống vào select đồ uống
    getDrinkList();
}
function onBtnSmallClick(){
    "use strict"
    var vSelectedMenuStructure = getSelectedMenu("S ( Small size)", 20, 1, 2, 200, 150000);
    vSelectedMenuStructure.displayMenuStructure();

    changeMenuButtonColor("S");
    gSelectedMenuStructure = vSelectedMenuStructure;
}
  //hàm thay đổi màu nút của Medium khi nhấn
function onBtnMediumClick(){
    "use strict"
    var vSelectedMenuStructure = getSelectedMenu("M ( Medium size)", 25, 4, 3, 300, 200000);
    vSelectedMenuStructure.displayMenuStructure();

    changeMenuButtonColor("M");
    gSelectedMenuStructure = vSelectedMenuStructure;
}
  //hàm thay đổi màu nút của Large khi nhấn
function onBtnLargeClick(){
    "use strict"
    var vSelectedMenuStructure = getSelectedMenu("L ( Large size)", 30, 8, 4, 500, 250000);
    vSelectedMenuStructure.displayMenuStructure();

    changeMenuButtonColor("L");
    gSelectedMenuStructure = vSelectedMenuStructure;
}
function onBtnOceanClick(){
    "use strict"
    gSelectedPizzaType = "Pizza Ocean Mania";
    console.log("%cLoại pizza được chọn là: " + gSelectedPizzaType,"color:blue");
    changeTypePizzaButtonColor("Ocean");
  }
  //Hàm đổi màu nút Pizza Hawaiian khi được chọn
  function onBtnHawaiClick(){
    "use strict"
    gSelectedPizzaType = "Pizza Hawaiian";
    console.log("%cLoại pizza được chọn là: " + gSelectedPizzaType,"color:blue");
    changeTypePizzaButtonColor("Hawaiian");
  }
  //Hàm đổi màu nút Pizza Cheesy Checken Bacon khi được chọn
  function onBtnBaconClick(){
    "use strict"
    gSelectedPizzaType = "Pizza Cheesy Checken Bacon";
    console.log("%cLoại pizza được chọn là: " + gSelectedPizzaType,"color:blue");
    changeTypePizzaButtonColor("Cheesy Checken Bacon");
  }
/***REGION 3 */
function onBtnGuiClick(){
    "use strict"
    console.log("%CNút Gửi vừa được bấm !!!", "color: red");
    getSelectedOder(gOder);
    if(kiemTraDataOrder(gOder)){
      displayOrder(gOder,gSelectedMenuStructure)
      $("#order-detail-modal").modal("show");
    }
  }
  function onBtnTaoDonClick(){
    "use strict";
    createDataOjectRequest();
  }


/***REGION 4 */
function getSelectedMenu(paramMenu, paramDuongKinh, paramSuonNuong, paramDrink, paramSalad, paramPrice){
    "use strict"
    var vSelectedMenuStructure = {
      menuName: paramMenu,    // S, M, L
      duongKinhCM: paramDuongKinh,
      suongNuong: paramSuonNuong,
      saladGr: paramSalad,
      drink: paramDrink,
      priceVND: paramPrice,
      displayMenuStructure(){
        console.log("%cMenu được chọn: " + this.menuName, "color:blue");
        console.log("Đường kính: " + this.duongKinhCM);
        console.log("Sườn nướng: " + this.suongNuong);
        console.log("Salad: " + this.saladGr);
        console.log("Drink: " + this.drink);
        console.log("Price: " + this.priceVND);
      }
    }
    return vSelectedMenuStructure;
}

function changeMenuButtonColor(paramBtn){
    "use strict"
    var vBtnSmall = $("#btn-small");
    var vBtnMeudim = $("#btn-medium");
    var vBtnLarge = $("#btn-large");

    if(paramBtn == "S"){
        vBtnSmall.removeClass("btn btn-danger").addClass("btn btn-success");
        vBtnMeudim.removeClass("btn btn-danger").addClass("btn btn-danger");
        vBtnLarge.removeClass("btn btn-danger").addClass("btn btn-danger");
    }else if(paramBtn == "M"){
        vBtnSmall.removeClass("btn btn-danger").addClass("btn btn-danger");
        vBtnMeudim.removeClass("btn btn-danger").addClass("btn btn-success");
        vBtnLarge.removeClass("btn btn-danger").addClass("btn btn-danger");

    }else if(paramBtn == "L"){
        vBtnSmall.removeClass("btn btn-danger").addClass("btn btn-danger");
        vBtnMeudim.removeClass("btn btn-danger").addClass("btn btn-danger");
        vBtnLarge.removeClass("btn btn-danger").addClass("btn btn-success");
    }
}
function changeTypePizzaButtonColor(paramBtn){
    "use strict"
    var vBtnOcean = $("#btn-ocean");
    var vBtnHawai = $("#btn-hawai");
    var vBtnBacon = $("#btn-bacon");

    if(paramBtn == "Ocean"){
        vBtnOcean.removeClass("btn btn-danger").addClass("btn btn-success");
        vBtnHawai.removeClass("btn btn-danger").addClass("btn btn-danger");
        vBtnBacon.removeClass("btn btn-danger").addClass("btn btn-danger");

    }else if(paramBtn == "Hawaiian"){
        vBtnOcean.removeClass("btn btn-danger").addClass("btn btn-danger");
        vBtnHawai.removeClass("btn btn-danger").addClass("btn btn-success");
        vBtnBacon.removeClass("btn btn-danger").addClass("btn btn-danger");

    }else if(paramBtn == "Cheesy Checken Bacon"){
        vBtnOcean.removeClass("btn btn-danger").addClass("btn btn-danger");
        vBtnHawai.removeClass("btn btn-danger").addClass("btn btn-danger");
        vBtnBacon.removeClass("btn btn-danger").addClass("btn btn-success"); 
    }
}
//GET DRINK TỪ SERVER
function getDrinkList() {
    "use strict";
    $.ajax({
        type: "GET",
        url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            selectDrinkLoad(response);
        },
        error: function(error){
            console.log(error.responseText);
        }
    });
}
  function selectDrinkLoad(paramResponse){
    "use strict"
    //var vDrinkList = JSON.parse(paramResponse.responseText);

    var vSelectDrink = document.getElementById("select-drink");
    for(var i = 0; i < paramResponse.length; i++){
      //tạo mới options
      var bDrinkOptinsElement = document.createElement("option");
      bDrinkOptinsElement.value = paramResponse[i].maNuocUong;
      bDrinkOptinsElement.text = paramResponse[i].tenNuocUong;
      //thêm options vừa tạo vào select
      vSelectDrink.appendChild(bDrinkOptinsElement);
    }
}
function getSelectedOder(paramOrder){
    "use strict"
    var vInpHoVaTen = document.getElementById("inp-fullname"); // truy xuất phần tử input thông qua thuộc tính id
    var vInpEmail = document.getElementById("inp-email");
    var vInpDienThoai = document.getElementById("inp-dien-thoai");
    var vInpDiaChi = document.getElementById("inp-dia-chi");
    var vInpLoiNhan = document.getElementById("inp-message");
    var vInpVoucherId = document.getElementById("inp-voucher");

    paramOrder.menuDuocChon = gSelectedMenuStructure; // gán giá trị cho thuộc tính của object paramOrder
    paramOrder.loaiPizza = gSelectedPizzaType;
    paramOrder.hoVaTen = vInpHoVaTen.value.trim();
    paramOrder.email = vInpEmail.value.trim();
    paramOrder.dienThoai = vInpDienThoai.value.trim();
    paramOrder.diaChi = vInpDiaChi.value.trim();
    paramOrder.loiNhan =  vInpLoiNhan.value.trim();
    paramOrder.voucherId = vInpVoucherId.value.trim();
}
function kiemTraDataOrder(paramOder){
    "use strict";
    if(paramOder.menuDuocChon.menuName == ""){
      alert("Bạn phải chọn Menu!");
      return false;

    }else if(paramOder.loaiPizza == ""){
      alert("Bạn phải chọn loại Pizza!");
      return false;
      
    }else if(!checkDrink()){
      alert("Bạn chưa chọn đồ uống!");
      return false;
    }
    else if(paramOder.hoVaTen == ""){
      alert("Bạn phải nhập họ và tên!");
      return false;

    }else if(!kiemTraEmail(paramOder.email)){
      alert("Email phải chứa @ và không bắt đầu hoặc kết thúc bằng @!")
      return false;

    }else if(paramOder.dienThoai == ""){
      alert("Bạn phải nhập số điện thoại!");
      return false;

    }else if(paramOder.diaChi == ""){
      alert("Bạn phải nhập địa chỉ!");
      return false;

    }else if(!checkVoucher(gOder)){
      return false;
    }
    return true;
}
function checkDrink(){
    var vIdDrink = document.getElementById("select-drink");
    var vDrinkValue = vIdDrink.value;
    if (vDrinkValue == 0){
      //alert("Chưa chọn nước uống");
      return false
    }
    return true;
}
//Hàm KTra Email
function kiemTraEmail(paramEmail){
    "use strict"
    if(!paramEmail.includes("@")){ // nếu email không chứ @ thì trả về giá trị false
      return false;

    }else if(paramEmail.startsWith("@")){// nếu email bắt đầu bằng @ thì trả về giá trị false
      return false;

    }else if(paramEmail.endsWith("@")){// nếu email kết thúc bằng @ thì trả về giá trị false
      return false;

    }
    return true;
}
function checkVoucher(paramOder){
    "use strict";
    var vVoucherId = paramOder.voucherId;
    $.ajax({
      type: "GET",
      url: "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + vVoucherId,
      async: false,
      dataType: "json",
      success: function (response,status) {
        console.log(response);
        console.log(status);
        gDiscount = response.phanTramGiamGia;
        return true;
      },
      error: function (error){
        console.log(error.responseText);
        gDiscount = 0;
        return true;
      }
    });
    return true;
}
// function checkVoucher(paramOder){
//     "use strict";
//     if(paramOder.voucherId == ""){
//       gDiscount = 0;
//       return true;
//     }else {  //Một số mã voucher đúng, bạn có thể dùng: 12354, 10056, 50516, 70651, 40381
//       const vBASE_URL =  "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/";
//       var vVoucherId = paramOder.voucherId;  //một số mã đúng để test: 95531, 81432,...lưu ý test cả mã sai
//       // nếu mã giảm giấ đã nhập, tạo vXmlHttp request và gửi về server
//       var vXmlHttp = new XMLHttpRequest();
//       vXmlHttp.open( "GET",  vBASE_URL + vVoucherId, false);
//       vXmlHttp.send();
//       if(vXmlHttp.status == gREQUEST_STATUS_OK) { // restFullAPI successful
//           // nhận lại response dạng JSON ở vXmlHttp.responseText và chuyển thành object
//           console.log(vXmlHttp.responseText); 
//           gDiscountVouchers = JSON.parse(vXmlHttp.responseText);
//           gDiscount = gDiscountVouchers.phanTramGiamGia;
//           return true;         
//       } 
//       else {
//           // không nhận lại được data do vấn đề gì đó: khả năng mã voucher ko dúng
//         //   alert("Mã voucher không đúng!");
//         //   var vDisplayDiv = document.getElementById("div-container-order");
//         //   vDisplayDiv.style.display = "none";
//         gDiscount = 0;
//         return true;
//       }
//     }
// }
function displayOrder(paramOder, paramCombo){
    "use strict"
    $("#inp-modal-fullname").val(paramOder.hoVaTen);
    $("#inp-modal-so-dien-thoai").val(paramOder.dienThoai);
    $("#inp-modal-dia-chi").val(paramOder.diaChi);
    $("#inp-modal-message").val(paramOder.loiNhan);
    $("#inp-modal-voucher").val(paramOder.voucherId);
    $("#tarea-info").val("Xác nhận: " + paramOder.hoVaTen + ", " + paramOder.dienThoai + ", " + paramOder.diaChi + ".\n"
    + "Combo: " + paramCombo.menuName + " ,Sườn nướng: " + paramCombo.suongNuong + " ,Nước: " + paramCombo.drink + "...\n"
    + "Loại Pizza: " + gSelectedPizzaType + ", Giá: " + paramCombo.priceVND + ", Mã giảm giá: " + paramOder.voucherId + ".\n"
    + "Phải thanh toán: " + paramOder.priceAnnualVND());
}
function createDataOjectRequest(){
    gObjectRequest.kichCo = gSelectedMenuStructure.menuName;
    gObjectRequest.duongKinh = gSelectedMenuStructure.duongKinhCM;
    gObjectRequest.suon = gSelectedMenuStructure.suongNuong;
    gObjectRequest.salad = gSelectedMenuStructure.saladGr;
    gObjectRequest.loaiPizza = gOder.loaiPizza;
    gObjectRequest.idVourcher = gOder.voucherId;
    var vDrink = document.getElementById("select-drink");
    gObjectRequest.idLoaiNuocUong = vDrink.value;
    gObjectRequest.soLuongNuoc = gSelectedMenuStructure.drink;
    gObjectRequest.hoTen = gOder.hoVaTen;
    gObjectRequest.thanhTien = gOder.priceAnnualVND;
    gObjectRequest.email = gOder.email;
    gObjectRequest.soDienThoai = gOder.dienThoai;
    gObjectRequest.diaChi = gOder.diaChi;
    gObjectRequest.loiNhan = gOder.loiNhan;

    $.ajax({
        type: "POST",
        url: "http://203.171.20.210:8080/devcamp-pizza365/orders",
        data: JSON.stringify(gObjectRequest),
        async: false,
        contentType: "application/json",
        success: function (response) {
            console.log(response);
            hienThiMaDonHang(response);
        },
        error: function(error){
            console.log(error.responseText);
        }
    });
}
function hienThiMaDonHang(paramResponse){
    "use strict";
    var vInpModalMaDonHang = $("#inp-modal-ma-don-hang");
    vInpModalMaDonHang.val(paramResponse.orderCode);
    $("#order-detail-modal").modal("hide");
    $("#create-order-modal").modal("show");
}